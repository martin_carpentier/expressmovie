class Movies {

    /**
     * Constructeur.
     */
    constructor() {  }


    /**
     * Méthode gérant l'affichage de la page d'accueil du module.
     * @param {IncomingMessage} req 
     * @param {ServerResponse} res 
     */
    getRoot(req, res) {
        const Movie = require("../models/Movie");
        Movie.find({}, (err, movies) => {
            if (err) {
                var opts = { movies: [] };
                console.error("error retreiving movies");
            } else {
                var opts = { movies: movies };
                console.log('movies', movies);
            }
            res.render("movies/root", opts);
        });
    }


    /**
     * Méthode gérant l'affichage du formulaire de visualisation d'un film.
     * @param {IncomingMessage} req 
     * @param {ServerResponse} res 
     */
    getView(req, res) {
        var id = req.params.param;
        const Movie = require("../models/Movie");
        Movie.findOne({_id: id}, (err, movie) => {
            if (err) {
                console.error("Error while retreiving movie: " + err);
                res.sendStatus(500);
            } else {
                res.statusCode = 200;
                console.log('movie', movie);
                res.render('movies/view', {movie: movie});
            }
        });
        
    }


    /**
     * Méthode gérant l'affichage du formulaire permettant d'éditer un film.
     * @param {IncomingMessage} req 
     * @param {ServerResponse} res 
     */
    getEdit(req, res) {
        var id = req.params.param;
        const Movie = require("../models/Movie");
        Movie.findOne({_id: id}, (err, movie) => {
            if (err) {
                console.error("Error while retreiving movie: " + err);
                res.sendStatus(500);
            } else {
                res.statusCode = 200;
                res.render("movies/edit", { movie: movie });
            }
        });
    }


    /**
     * Méthode gérant la réception du formulaire d'édition de film.
     * @param {IncomingMessage} req 
     * @param ServerResponse} res 
     */
    postEdit(req, res) {
        const Movie = require("../models/Movie");
        Movie.findOne({_id: req.body.id}, (err1, movie) => {
            if (err1) {
                console.error("Error while retreiving movie: " + err1);
                res.sendStatus(500);
            } else {
                movie.title = req.body.title;
                movie.year = req.body.year;
                movie.style = req.body.style;
                movie.actor = req.body.actor;
                movie.save((err2, savedMovie) => {
                    if (err2) {
                        console.error("Error while saving movie: " + err2);
                        res.sendStatus(500);
                    } else {
                        res.writeHead(301, {Location: "/movies/view/" + savedMovie.id});
                        res.end();
                    }
                });
            }
        });
    }


    /**
     * Méthode gérant l'affichage du formulaire d'ajout d'un film.
     * @param {IncomingMessage} req 
     * @param {ServerResponse} res 
     */
    getAdd(req, res) {
        res.render("movies/add");
    }


    /**
     * Méthode gérant la réception du formulaire d'ajout d'un film.
     * @param {IncomingMessage} req 
     * @param {ServerResponse} res 
     */
    postAdd(req, res) {
        const Movie = require("../models/Movie");
        var myMovie = new Movie(req.body);
        myMovie.save((err, savedMovie) => {
            if (err) {
                console.error("Error while saving movie: " + err);
                res.sendStatus(500);
            } else {
                res.writeHead(301, {Location: "/movies/view/" + savedMovie.id});
                res.end();
            }
        });
    }


    /**
     * Méthode gérant l'affichage du formulaire de suppression.
     * @param {IncomingMessage} req 
     * @param {ServerResponse} res 
     */
    getDelete(req, res) {
        var id = req.params.param;
        const Movie = require("../models/Movie");
        Movie.findOne({_id: id}, (err, movie) => {
            if (err) {
                console.error("Error while retreiving movie: " + err);
                res.sendStatus(500);
            } else {
                res.render("movies/delete", { movie: movie });
            }
        });
    }


    /**
     * Méthode gérant la réception du formulaire de suppression.
     * @param {IncomingMessage} req 
     * @param ServerResponse} res 
     */
    postDelete(req, res) {
        var id = req.body.id;
        const Movie = require("../models/Movie");
        Movie.findOne({_id: id}, (err1, movie) => {
            if (err1) {
                console.error("Error while retreiving movie: " + err1);
                res.sendStatus(500);
            } else {
                movie.remove((err2) => {
                    if (err2) {
                        console.error("Error while deleting movie: " + err2);
                        res.sendStatus(500);
                    } else {
                        res.writeHead(301, {Location: "/movies"});
                        res.end();
                    }
                });
            }
        });
    }


}

// Ligne obligatoire si je veux être capable de caller la classe Movies dans app.js
module.exports = Movies;