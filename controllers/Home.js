class Home {

    /**
     * Méthode gérant la page d'accueil du module.
     * @param {IncomingMessage} req 
     * @param {ServerResponse} res 
     */
    getRoot(req, res) {
        res.send("res.contructor.name = " + res.constructor.name);
        // res.render("home/home");
    }
}

module.exports = Home;