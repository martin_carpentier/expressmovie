const bodyParser = require("body-parser");
const express = require("express");
const fs = require("fs");
const mongoose = require("mongoose");

const PORT = 8080;

const app = express();
app.set("controllers", "./controllers");

// Tentative de connexion à MongoDB
mongoose.connect("mongodb://expmovies_user:CMrUi2tXPqEDGtT@ds255889.mlab.com:55889/expressmovies", { useNewUrlParser: true });
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: cannot connect to DB"));
db.once("open", () => { console.log("connected to the DB"); });

// Middleware: pour dire à ExpressJS où et comment servir les fichiers statics.
app.use("/public", express.static("public"));

// Middleware: pour être capable d'analyser les variables d'un formulaire post.
app.use(bodyParser.urlencoded({ extended: false }));

// Pour déclarer et utiliser le Template Engine.
app.set("views", "./views");
app.set("view engine", "ejs");

/**
 * Fonction permettant de uppercaser la première lettre d'une chaine de caractères.
 * @param {String} str
 * @returns {String}
 */
function ucfirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * Méthode permettant d'appeler l'action appropriée dans le bon controller en fonction de se qu'on a comme path dans l'URL.
 * @param {*} req 
 * @param {*} res 
 */
function doAction(req, res) {
    var method = req.method.toLowerCase();
    var module = req.params.module ? ucfirst(req.params.module) : 'Home';
    var action = req.params.action ? ucfirst(req.params.action) : 'Root';
    
    if (!module) { res.sendStatus(404); }

    var path = __dirname+"/" + app.get("controllers") + "/"+module+".js";
    if (fs.existsSync(path)) {
        const cls = require(path);
        module = new cls();

        action = method+action;
        if (typeof module[action] != "undefined") {
            module[action](req, res);
        } else {
            res.sendStatus(404);
        }
    } else {
        res.sendStatus(404);
    }
}

// Path appelé lorqu'on a qq'chose comme /movies/view/4 dans l'URL.
app.all("/:module/:action/:param", (req, res) => {
    doAction(req, res);
});

// Path appelé lorqu'on a qq'chose comme /movies/add dans l'URL.
app.all("/:module/:action", (req, res) => {
    doAction(req, res);
});

// Path appelé lorqu'on a qq'chose comme /movies dans l'URL.
app.all("/:module", (req, res) => {
    doAction(req, res);
});

// Path de la page d'accueil.
app.get("/", (req, res) => {
    doAction(req, res);
});

// On indique à ExpressJS d'écouter sur le port 8080
app.listen(PORT, () => { 
    console.log(`listening on port ${PORT}`);
 });
