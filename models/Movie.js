var mongoose = require('mongoose');

// On crée le modèle "Movie" à partir d'un schema.
var Movie = mongoose.model('Movie', mongoose.Schema({ 
    'title': String,
    'year': Number,
    'style': String,
    'actor': String
}));

// On rend ce modèle disponible à travers l'application.
module.exports = Movie;